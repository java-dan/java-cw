package lesson01;

public class Snippet1 {
    public static void main(String[] args) {
        short x = 32760;
        short y = 10;
        short z = (short)(x + y);
        System.out.println(z);

        int a = 32760;
        int b = 10;
        int c = a + b;
        System.out.println(c);
    }

}
