package lesson02;

public class Output2 {
    public  static void print(String line) {
        System.out.println(line);
    }
    public static void main(String[] args) {
        print("whatever");
//        print(1);  error
        print(String.format("%d + %d = %d\n", 2, 3, 2 + 3));

        ScannerConstrained sc = new ScannerConstrained();
        String line = sc.nextLine();
        System.out.println(line);

    }
}
