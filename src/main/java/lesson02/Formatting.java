package lesson02;

public class Formatting {
    public static void main(String[] args) {
        String s1 = "Hello ";
        String s2 = "Jim";

        String message1 = s1 + "," + s2 + "!";
        System.out.println(message1);
        String message2 = String.format("%s, %s!", s1, s2);
        System.out.println(message2);

        double x = 1.23334355355354;
        System.out.println(x);

        String message = String.format("%f", x);
        System.out.println(message);

        System.out.println();
        System.out.printf("%.3f", x);
        System.out.println();

        String message3 = String.format("%1$s, %1$s, %1$s", x);
        System.out.println(message3);

        String message4 = String.format("%2$s, %1$s, %1$s", s1, s2);
        System.out.println(message4);

    }
}
