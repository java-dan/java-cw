package lesson02;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class LearnRandom3 {
    public static String arrayTOString(int[] xs) {
        return Arrays.toString(xs);
    }
    public static void main(String[] args) {
        Random random = new Random();
        IntStream intStream1 = random.ints(10, 100);
        IntStream intStream2 = intStream1.limit(20);
        int[] ints = random.ints().limit(20).toArray();
        int[] ints2 = intStream2.toArray();

        System.out.println(ints);
        System.out.println(ints2);

        String line = arrayTOString(ints2);
        System.out.println(line);
    }
}
