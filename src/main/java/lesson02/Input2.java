package lesson02;

import java.io.InputStream;
import java.util.Scanner;

public class Input2 {
    public static void main(String[] args) {
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);

        System.out.println("Enter your name: ");

        String line = scanner.nextLine();
        System.out.print("'");
        System.out.print(line);
        System.out.print("'");
        System.out.println();

        String message = "Hello, " + line + "!";
        System.out.println(message);
    }
}
