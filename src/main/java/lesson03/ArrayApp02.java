package lesson03;

public class ArrayApp02 {
    public static void main(String[] args) {
        int[] a = new int[10];
        var b = new double[10]; //double[] b = new double[10];

        String s1 = "0110abc";
        char[] chars1 = s1.toCharArray(); // ['0', '1', '1', '0', 'a, 'b' , 'c']
        String s2 = "0110";
        char[] chars2 = s2.toCharArray(); // ['0', '1', '1', '0']
        int[] ints = new int[chars1.length];
        for (int i = 0; i < chars1.length; i++) {
//            char cc1 = chars1[0]; // '0'
//            int cc2 = cc1 - 48; // cc1 - '0'
//            ints[i] = cc2;

            ints[i] = chars1[0] - '0';
        }

    }
}
