package lesson07;

public class Pizza {
        String name;
        Integer size;


        public Pizza(String name, int size) {
            this.name = name;
            this.size = size;
        }
        @Override
        public String toString() { return String.format("Pizza: '%s' of size %d", name, size); }

        @Override
        public boolean equals(Object that0) {
           if(that0 == null) return false;
           if(this == that0) return true;

//           if(!(that0 instanceof Pizza)) return false;
//           Pizza that = (Pizza) that0;
           if(!(that0 instanceof Pizza that)) return false;

//           if(!this.name.equals(that.name)) return false;
//           if(!this.size.equals(that.size)) return false;
//
//           return true;
            return this.name.equals(that.name) &&
                   this.size.equals(that.size);

        }

}
