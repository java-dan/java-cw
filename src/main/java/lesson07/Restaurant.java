package lesson07;

public class Restaurant {
    public static void main(String[] args) {
        Pizza p1 = new Pizza("Margarita", 60);
        Pizza p2 = new Pizza("Margarita", 60);
        Pizza17 p17a = new Pizza17("Margarita", 60);
        Pizza17 p17b = new Pizza17("Margarita", 60);
        Pizza17 p17c = new Pizza17("Margarita", 60);

        System.out.println(p17a.equals(p17b));
        System.out.println(p17a.equals(p17c));
        System.out.println(p17a);
        System.out.println("-".repeat(20));
    }
}
